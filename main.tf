module "base" {
  source = "git::https://gitlab.com/stemid-iac/terraform/modules/base-ignition.git"
}

module "network" {
  source = "git::https://gitlab.com/stemid-iac/terraform/modules/network-ignition.git"
  hostname = var.instance.name
  connections = var.instance.connections
}

module "serviceusers" {
  source = "git::https://gitlab.com/stemid-iac/terraform/modules/serviceuser-ignition.git"
  count = length(var.instance.serviceusers)
  serviceuser = var.instance.serviceusers[count.index]
}

module "node_exporter" {
  source = "git::https://gitlab.com/stemid-iac/terraform/modules/node-exporter-ignition.git"
  home = var.instance.node_exporter.home
  uid = var.instance.node_exporter.uid
  gid = var.instance.node_exporter.gid
}

data "ignition_config" "config" {
  merge {
    source = "data:text/plain;base64,${base64encode(module.base.config.rendered)}"
  }
  merge {
    source = "data:text/plain;base64,${base64encode(module.network.config.rendered)}"
  }
  dynamic "merge" {
    for_each = module.serviceusers
    content {
      source = "data:text/plain;base64,${base64encode(merge.value.config.rendered)}"
    }
  }
  merge {
    source = "data:text/plain;base64,${base64encode(module.node_exporter.config.rendered)}"
  }
}
