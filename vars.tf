variable "instance" {
  type = object({
    name = string
    connections = list(any)
    serviceusers = list(object({
      username = string
      home = string
      uid = number
      gid = number
    }))
    node_exporter = object({
      home = string
      uid = number
      gid = number
    })
    podman_backup_config = optional(any)
    powerdns_settings = optional(any)
  })
}
