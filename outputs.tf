output "sshkey" {
  value = module.base.sshkey
}

output "config" {
  value = data.ignition_config.config
}
